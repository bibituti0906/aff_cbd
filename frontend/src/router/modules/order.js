/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const OrderRoute = {
  path: '/order',
  component: Layout,
  name: 'Order',
  noCache: true,
  meta: {
    title: 'Order',
    icon: 'user'
  },
  children: [
    {
      path: '',
      component: () => import('@/views/order/index'),
      name: 'Order',
      meta: { title: 'Order', icon: 'get-money', breadcrumb: false, noCache: true }
    },
    {
      path: '/dropship/view/:id',
      component: () => import('@/views/order/view'),
      name: 'OrderView',
      hidden: true,
      meta: { title: 'Order View', icon: 'user' }
    }
  ]
}
export default OrderRoute
