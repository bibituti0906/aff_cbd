/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const UserRoute = {
  path: '/user',
  component: Layout,
  name: 'User',
  meta: {
    title: 'User',
    icon: 'user',
    roles: ['admin']
  },
  children: [
    {
      path: '',
      component: () => import('@/views/user/index'),
      name: 'User',
      meta: { title: 'User', icon: 'user', roles: ['admin'] }
    }
  ]
}
export default UserRoute
