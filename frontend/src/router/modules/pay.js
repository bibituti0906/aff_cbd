/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const PayRoute = {
  path: '/pay',
  component: Layout,
  name: 'Pay',
  noCache: true,
  meta: {
    title: 'Pay',
    icon: 'user',
    roles: ['admin']
  },
  children: [
    {
      path: '',
      component: () => import('@/views/pay/index'),
      name: 'Pay',
      meta: { title: 'Pay', icon: 'get-money', breadcrumb: false, noCache: true, roles: ['admin'] }
    }
  ]
}
export default PayRoute
