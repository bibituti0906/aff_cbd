import request from '@/utils/request'

const api_url = '/order'
export function listData(params) {
  return request({
    url: `${api_url}`,
    method: 'get',
    params: params
  })
}

export function listNote(id) {
  return request({
    url: `${api_url}/note/${id}`,
    method: 'get'
  })
}

export function getDashboard() {
  return request({
    url: `${api_url}/dashboard`,
    method: 'GET'
  })
}

export function chartOrderStatus() {
  return request({
    url: `${api_url}/dashboard/orderStatus`,
    method: 'GET'
  })
}

export function chartToDay() {
  return request({
    url: `${api_url}/dashboard/chartToDay`,
    method: 'GET'
  })
}

export function payMonth(params) {
  return request({
    url: `${api_url}/pay`,
    method: 'Get',
    params: params
  })
}

export function payNow(data) {
  return request({
    url: `${api_url}/pay/payNow`,
    method: 'Post',
    data: data
  })
}

export function viewOrderFormTo(id, params) {
  return request({
    url: `${api_url}/pay/viewOrderFormTo/${id}`,
    method: 'Get',
    params: params
  })
}

export function payUser(data) {
  return request({
    url: `${api_url}/pay/payUser`,
    method: 'Post',
    data: data
  })
}
