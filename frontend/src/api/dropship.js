import request from '@/utils/request'

const api_url = '/dropship'
export function listData(params) {
  return request({
    url: `${api_url}`,
    method: 'get',
    params: params
  })
}

export function viewData(id) {
  return request({
    url: `${api_url}/view/${id}`,
    method: 'get',
  })
}

export function updateData(id, data) {
  return request({
    url: `${api_url}/update/${id}`,
    method: 'put',
    data: data
  })
}

export function createNote(id, data) {
  return request({
    url: `${api_url}/createNote/${id}`,
    method: 'post',
    data: data
  })
}

export function listNote(id) {
  return request({
    url: `${api_url}/listNote/${id}`,
    method: 'GET'
  })
}

export function dashboard() {
  return request({
    url: `${api_url}/dashboard`,
    method: 'GET'
  })
}

export function dropShipChart() {
  return request({
    url: `${api_url}/chart`,
    method: 'GET'
  })
}
