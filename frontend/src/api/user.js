import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/user/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/user/info',
    method: 'get'
  })
}

export function listUser(params = {}) {
  console.log(params)
  return request({
    url: '/user',
    method: 'get',
    params: params
  })
}

export function saveUser(data) {
  return request({
    url: '/user/create',
    method: 'post',
    data
  })
}

export function deleteUser(id) {
  return request({
    url: `/user/destroy/${id}`,
    method: 'delete'
  })
}
export function updateUser(id, data) {
  return request({
    url: `/user/update/${id}`,
    method: 'put',
    data
  })
}

export function createApiKey() {
  return request({
    url: `/user/createApiKey`,
    method: 'get'
  })
}

export function logout() {
  return request({
    url: '/vue-element-admin/user/logout',
    method: 'post'
  })
}


export function updateProfile(data) {
  return request({
    url: '/user/updateProfile',
    method: 'put',
    data: data
  })
}

export function reportMe(params) {
  console.log(params)
  return request({
    url: '/user/info/report',
    method: 'get',
    params: params
  })
}
