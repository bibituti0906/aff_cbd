<?php
/**
 * Plugin Name:       The SWISS HEMP AFF Plugin
 * Plugin URI:        https://theswisshemp.com/
 * Description:       Connection Wooecommerce for TheSwissHemp Aff System
 * Version:           1.0.0
 * Author:            Tuan Linh
 * Author URI:        theswisshemp.com
 * Text Domain:       laverteshop-dropship
 * Domain Path:       /languages
 *
 * @package           weed-aff
 *
 *
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */
define( 'WEED_AFF', '1.0.0' );
define('URL_API', 'http://localhost:8000');
const MAP_STATUS = [
	'pending' => 0,
	'on-hold' => 4,
	'processing' => 2,
	'cancelled' => 5,
	'refunded' => 6,
	'failed' => 1,
	'completed' => 3
];
function activate_weed_aff() {

}

function deactivate_weed_aff() {

}
register_activation_hook( __FILE__, 'activate_weed_aff' );
register_deactivation_hook( __FILE__, 'deactivate_weed_aff' );
function callTracking($order_id) {
	$order = new WC_Order( $order_id );
	$total = $order->get_total();
	$total_tax = $order->get_total_tax();
	$total_refund = $order->get_total_refunded();
	$currency_code = $order->get_currency();
	$order_status  = $order->get_status();
	$dataBuild = [
		'vendor_id' => $order_id,
		'order_total' => $total,
		'order_tax' => $total_tax,
		'order_refund' => $total_refund,
		'currency_code' => $currency_code,
		'order_status' => MAP_STATUS[$order_status],
		'order_status_real' => $order_status
	];
	$ch = curl_init(URL_API.'/api/order/trackingOrder/'.$order_id);
	curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($dataBuild) );
	curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	$result = curl_exec($ch);
	curl_close($ch);
	if($result) {
		$dataRes = json_decode($result, true);
		if ($dataRes['status'] === true) {

		}
	}
}
add_action('woocommerce_thankyou', 'aff_weed_new_order', 10, 1);
function aff_weed_new_order($order_id) {
	$order = new WC_Order( $order_id );
	$total = $order->get_total();
	$total_tax = $order->get_total_tax();
	$total_refund = $order->get_total_refunded();
	$currency_code = $order->get_currency();
	$order_status  = $order->get_status();
	$coupon_codes   = $order->get_coupon_codes();
	$user = $order->get_user();
	$user_id = $user->data->ID;
	if(isset($_COOKIE['ref_aff']) || !empty($coupon_codes)) {
		$dataBuild = [
			'vendor_id' => $order_id,
			'order_total' => $total,
			'order_tax' => $total_tax,
			'order_refund' => $total_refund,
			'currency' => $currency_code,
			'order_status' => MAP_STATUS[$order_status],
			'country_code' => $order->get_billing_country(),
			'first_name' => $order->get_billing_first_name(),
			'last_name' => $order->get_billing_last_name(),
			'ref' => $_COOKIE['ref_aff'],
			'coupon' => $coupon_codes,
			'customer_id' => $user_id
		];
		if(isset($_COOKIE['ref_aff'])) {
			$dataBuild['ref'] = $_COOKIE['ref_aff'];
		}
		if(!empty($coupon_codes)) {
			$dataBuild['coupon'] = $coupon_codes;
		}
		$ch = curl_init(URL_API.'/api/order/create');
		curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($dataBuild) );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		$result = curl_exec($ch);
		curl_close($ch);
		if($result) {
			$dataRes = json_decode($result, true);
			if ($dataRes['status'] === true) {

			}
		}
	}
}

add_action( 'woocommerce_order_refunded', 'aff_weed_woocommerce_order_refunded', 10, 2 );
add_action( 'woocommerce_refund_deleted', 'aff_weed_woocommerce_delete_order_refunded', 10, 2 );
// Do the magic
function aff_weed_woocommerce_order_refunded( $order_id, $refund_id )
{
	callTracking($order_id);
}
function aff_weed_woocommerce_delete_order_refunded( $refund_id, $order_id )
{
	callTracking($order_id);
}

function aff_weed_action_woocommerce_order_status_changed( $order_id, $status_from ) {
    callTracking($order_id);
};
// add the action
add_action( 'woocommerce_order_status_changed', 'aff_weed_action_woocommerce_order_status_changed', 10, 2 );
function aff_weed_action_woocommerce_update_order( $order_id ) {
    callTracking($order_id);
};

//add_action( 'init', 'weed_add_cookie');
//function weed_add_cookie() {
//	if(isset($_GET['ref'])) {
//		$res = wp_remote_get(URL_API.'/api/user/ref/'.$_GET['ref']);
//		if (!is_wp_error($res)) {
//			$dataBody = json_decode($res['body'], true);
//			if($dataBody['status'] == true) {
//				if(isset($_COOKIE['ref_aff'])) {
//					unset($_COOKIE['ref_aff']);
//				}
//				setcookie("ref_aff", $_GET['ref'], time()+ 60*60*24* $dataBody['data']['cookie_duration'], '/', $_SERVER['HTTP_HOST']);
//
//			}
//		}
//	}
//}

add_action( 'init', 'weed_add_cookie');
function weed_add_cookie() {
    if(isset($_GET['ref'])) {
        $formData = [
            'ip' => get_ip_aff_be(),
            'user_agent' => isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '',
            'ref_url' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '',
        ];
        $res = wp_remote_get(URL_API.'/api/user/ref/'.$_GET['ref'].'?'.http_build_query($formData));
        if (!is_wp_error($res)) {
            $dataBody = json_decode($res['body'], true);
            if($dataBody['status'] == true) {
                if(isset($_COOKIE['ref_aff'])) {
                    unset($_COOKIE['ref_aff']);
                }
                setcookie("ref_aff", $_GET['ref'], time()+ 60*60*24* $dataBody['data']['cookie_duration'], '/', $_SERVER['HTTP_HOST']);

            }
        }
    }
}

function get_ip_aff_be() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}




