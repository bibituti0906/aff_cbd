<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\User;
use Modules\Order\Entities\Order;
use Modules\User\Entities\UserAffLog;
use Carbon\Carbon;

class ReportController extends Controller
{
    public function me()
    {
        $to = \request('to', date('Y-m-d'));
        $from = \request('from', '');
        if ($from == '') {
            $from = Carbon::now()->subDays(30)->format('Y-m-d');
        }
        $viewTotal = UserAffLog::where('report_date', '>=', $from)->where('report_date', '<=', $to)
                                ->where('user_id', auth()->user()->id)
                                ->select(\DB::raw('sum(total_view) as total_view'))
                                ->first();
        $viewTotal = ($viewTotal->total_view != '') ? $viewTotal->total_view : 0;
        $orderReport = Order::select( \DB::raw('sum(order_commission) as total_commission'), \DB::raw("count(*) as total_order"))
                            ->where('created_at', '>=', $from)
                            ->where('created_at', '<=', $to. ' 23:59:59')
                            ->where('order_status', Order::COMPLETTED_PAYMENT)
                            ->first();
        $total_commission = $total_order = 0;
        if ($orderReport) {
            $total_commission = ($orderReport->total_commission != '') ? $orderReport->total_commission : 0 ;
            $total_order = ($orderReport->total_order != '') ? $orderReport->total_order : 0;
        }
        return [
            'status' => true,
            'data' => [
                'view_total' => $viewTotal,
                'total_commission' => $total_commission,
                'total_order' => $total_order,
                'from' => $from,
                'to' => $to
            ]
        ];
//        $user = auth()->user();
//        $visit = $user->visit;
//        $customer = $user->customer;
//        $dataReport = [];
//        for($i=1; $i < 13; $i++) {
//            $dataReport[$i] = [
//              'month' => $i,
//              'total' => 0
//            ];
//        }
//        $order = Order::select( \DB::raw('sum(order_commission) as total'), \DB::raw("Month(created_at) as month"))
//            ->where('created_at', '>=', date('Y-01-01'))
//            ->where('created_at', '<=', date('Y-12-31').' 23:59')
//            ->where('order_status', Order::COMPLETTED_PAYMENT)
//            ->groupBy('month');
//        $order = $order->where('user_id', auth()->user()->id)->get();
//        if($order->isNotEmpty()) {
//            foreach ($order as $row) {
//                $dataReport[$row->month]['total'] = $row->total;
//            }
//        }
//        return [
//          'status' => true,
//          'data' => [
//              'visit' => $visit,
//              'customer' => $customer,
//              'dataReport' => array_reverse($dataReport)
//          ]
//        ];

    }

}
