<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\User\Http\Requests\LoginFormRequest;
use App\Services\RecaptchaService;

class AuthController extends Controller
{
    public function login(LoginFormRequest $request)
    {
        $formRequest = $request->only('email', 'password');
        $checkCapcha = app(RecaptchaService::class)->validate($request->recapcha);
        if ($checkCapcha['success'] === true && $checkCapcha['score'] >= 0.5) {
            if (! $token = \Auth::guard('api')->attempt($formRequest)) {
                return response()->json(['status' => false, 'msg' => 'Unauthorized'], 401);
            }
            return $this->respondWithToken($token);
        }
        return response()->json(['status' => false, 'msg' => 'Not validate Captcha'], 401);
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ]);
    }
}
