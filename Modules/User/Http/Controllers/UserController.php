<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\User;
use Modules\User\Entities\UserToken;
use Modules\User\Http\Requests\UserFormRequest;
use Modules\User\Entities\UserCustomer;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $email = request('email', '');
        $userStatus = request('user_status', '');
        $userLevel = request('user_level', '');
        $users = User::orderBy('id','DESC');
        if ($email != '') {
            $users->where('email', 'Like', '%'.$email.'%');
        }
        if($userLevel != '') {
            $users->where('user_level', $userLevel);
        }
        if($userStatus != '') {
            $users->where('user_status', $userStatus);
        }
        $users = $users->paginate();
        return ['status' => true, 'data' => $users];
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(UserFormRequest $request)
    {
        $user = new User;
        $dataForm = $request->all();
        $dataForm['password'] = \Hash::make($dataForm['password']);
        $user->fill($dataForm);
        $user->save();
        return ['status' => true, 'user' => $user];
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        $dataUser = $user->toArray();
        $dataUser['roles'] = ($user->user_level == User::LEVEL_USER_ADMIN) ? ['admin'] : ['partner'];
        return ['status' => true, 'data' => $dataUser];
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(UserFormRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $dataForm = $request->all();
        if(array_key_exists('password', $dataForm) && $dataForm['password'] != '') {
            $dataForm['password'] = \Hash::make($dataForm['password']);
        }
        $user->fill($dataForm);
        $user->save();
        return ['status' => true];
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return ['status' => true];
    }

    public function infoUser()
    {
        $user = auth('api')->user();
        $dataUser = $user->toArray();
        $dataUser['roles'] = ($user->user_level == User::LEVEL_USER_ADMIN) ? ['admin'] : ['partner'];
        return ['status' => true, 'data' => $dataUser ];
    }

    public function profileUpdate()
    {
        $user = auth('api')->user();
        if(request('password', '') != '') {
            $user->password = \Hash::make(request('password'));
            $user->save();
        }
        return ['status' => true];
    }

    public function apiKey()
    {
        $apiKey = '';
        while(true) {
            $apiKey = \Str::random(10);
            $count = User::where('ref_code', $apiKey)->count();
            if ($count == 0) {
                break;
            }
        }
        return ['status' => true, 'apiKey' => $apiKey];
    }

    public function infoCookie($ref)
    {
        $user = User::where('ref_code', $ref)->first();
        if ($user) {
            $user->visit = $user->visit + 1;
            if(request('customer_id', '') != '')  {
                $userCustomer = UserCustomer::where('customer_id', request('customer_id'))->count();
                if ($userCustomer) {
                    $userCustomer = new UserCustomer();
                    $userCustomer->fill([
                        'user_id' => $user->id,
                        'customer_id' => request('customer_id')
                    ]);
                    $userCustomer->save();
                    $user->customer = $user->customer + 1;
                }
            }
            $user->save();
            file_put_contents(__DIR__.'/data.log', json_encode(request()->all()));
            dispatch(new \App\Jobs\LogUserRefJob($user->id, [
                'ip' => request('ip', ''),
                'user_agent' => request('user_agent', ''),
                'ref_url' => request('ref_url', ''),
            ]));
            return ['status' => true, 'data' => [
               'cookie_duration' => $user->cookie_duration
            ]];
        }
        return ['status' => false];
    }
}
