<?php

namespace Modules\User\Http\Controllers;

use App\Services\RecaptchaService;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\User\Http\Requests\ResetpassFormRequest;
use Modules\User\Http\Requests\ChangePassFormRequest;
use App\Models\User;
use Illuminate\Support\Str;
use Mailgun\Mailgun;

class ResetpassController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    public $privateKey = 'jD+mCZr+y/9wVVEf';

    public function reset(ResetpassFormRequest $request)
    {
        $formRequest = $request->only('email');
        $checkCapcha = app(RecaptchaService::class)->validate($request->recapcha);
        if ($checkCapcha['success'] === true && $checkCapcha['score'] >= 0.5) {
            $user = User::where('email', $formRequest['email'])->first();
            if ($user) {
                $ramdonstring = Str::random(10);
                $time = time();
                $hash = hash_hmac('md5', $user->email.$ramdonstring.$time, $this->privateKey);
                $buildData = [
                  'email' => $user->email,
                  'access_key' => $ramdonstring,
                  'hash' => $hash,
                  'time' => $time
                ];
                $html = view('mail.reset', ['httpbuild' => http_build_query($buildData)])->render();
                $mailgun = Mailgun::create(config('mailgun.api_key'), 'https://api.eu.mailgun.net');
                $send = $mailgun->messages()->send('theswisshemp.com', [
                    'from'    => 'THESWISSHEMP<noreply@theswisshemp.com>',
                    'to'      => $user->email,
                    'subject' => 'Reset your Affiliate Theswisshemp password',
                    'html' => $html
                ]);
                return ['status' => true, 'msg' => 'Password reset is successful, Reset link has been sent, check your email to re-enter your password', 'id' => $send];
            }
            return ['status' => false, 'msg' => 'Account not exits'];
        }
        return ['status' => false, 'msg' => 'Capcha not validate'];
    }

    public function changepass(ChangePassFormRequest $request)
    {
        $checkCapcha = app(RecaptchaService::class)->validate($request->recapcha);
        $msg = '';
        if ($checkCapcha['success'] === true && $checkCapcha['score'] >= 0.5) {
            $dataForm = $request->all();
            $time = $dataForm['time'];
            $hash = $dataForm['hash'];
            $email = $dataForm['email'];
            $password = $dataForm['password'];
            $access_key = $dataForm['access_key'];
            $hashCheck = hash_hmac('md5', $email.$access_key.$time, $this->privateKey);
            if (hash_equals($hashCheck, $hash) ) {
                $user = User::where('email', $email)->first();
                if ($user) {
                    $user->password = \Hash::make($password);
                    $user->save();
                    return ['status' => true, 'msg' => 'Change Password Success'];
                }
                $msg = 'User Not Found';
            } else {
                $msg = 'Hash Check Not Validate';
            }
        } else {
            $msg = 'Google Captcha Not Validate';
        }
        return ['status' => false, 'msg' => $msg];
    }
}
