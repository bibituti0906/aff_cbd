<?php

namespace Modules\User\Http\Requests;

use App\Http\Requests\BaseFormRequest;

class UserFormRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'email' => 'required|email|unique:users',
            'name' => 'required|min:3',
            'password' => 'required'
        ];
        if ($this->route('id')) {
            if($this->password == '') {
                unset($rules['password']);
            }
            unset($rules['email']);
        }
        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
