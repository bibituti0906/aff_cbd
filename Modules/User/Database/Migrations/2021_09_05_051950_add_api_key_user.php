<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApiKeyUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {        
        Schema::table('users', function (Blueprint $table) {
            $table->tinyInteger('user_status')->default(1)->index();
            $table->tinyInteger('user_level')->default(1)->index();
			$table->char('ref_code',50)->unique();			
			$table->char('coupon_referral',50)->index();
			$table->integer('cookie_duration');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {     
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('user_status');
            $table->dropColumn('user_level');
			$table->dropColumn('ref_code');
			$table->dropColumn('coupon_referral');
			$table->dropColumn('cookie_duration');
        });
    }
}
