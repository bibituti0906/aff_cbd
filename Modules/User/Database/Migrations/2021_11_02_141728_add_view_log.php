<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddViewLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_aff_logs', function ($table) {
            $table->id();
            $table->date('report_date')->index();
            $table->bigInteger('user_id')->index();
            $table->bigInteger('total_view');
            $table->timestamps();
        });
        Schema::create('user_aff_log_datas', function ($table) {
            $table->id();
            $table->bigInteger('report_id')->index();
            $table->bigInteger('user_id')->index();
            $table->string('ip')->nullable();
            $table->text('user_agent')->nullable();
            $table->text('ref_url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('', function (Blueprint $table) {

        });
    }
}
