<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('user')->group(function() {
    Route::post('/login', 'AuthController@login');
    Route::post('/reset', 'ResetpassController@reset');
    Route::post('/changepass', 'ResetpassController@changepass');
    Route::get('/ref/{ref}', 'UserController@infoCookie');
    Route::middleware(['auth.jwt'])->group(function() {
        Route::middleware(['check_admin'])->group(function() {
            Route::get('/', 'UserController@index');
            Route::post('/create', 'UserController@store');
            Route::get('/view/{id}', 'UserController@show');
            Route::put('/update/{id}', 'UserController@update');
            Route::delete('/destroy/{id}', 'UserController@destroy');
            Route::get('/createApiKey', 'UserController@apiKey');
        });
        Route::get('/info', 'UserController@infoUser');
        Route::get('/info/report', 'ReportController@me');
        Route::put('/updateProfile', 'UserController@profileUpdate');
    });
});
