<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserAffLog extends Model
{
    use HasFactory;

    protected $table = 'user_aff_logs';

    protected $fillable = [
        'report_date',
        'user_id',
        'total_view'
    ];

    protected static function newFactory()
    {
        return \Modules\User\Database\factories\UserAffLogFactory::new();
    }
}
