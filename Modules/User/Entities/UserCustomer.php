<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserCustomer extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'customer_id'
    ];

    protected static function newFactory()
    {
        return \Modules\User\Database\factories\UserCustomerFactory::new();
    }
}
