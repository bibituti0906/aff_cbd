<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserAffLogData extends Model
{
    use HasFactory;

    protected $table = 'user_aff_log_datas';

    protected $fillable = [
        'report_id',
        'user_id',
        'ip',
        'user_agent',
        'ref_url'
    ];

    protected static function newFactory()
    {
        return \Modules\User\Database\factories\UserAffLogDataFactory::new();
    }
}
