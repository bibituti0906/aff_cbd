<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\User;

class UserToken extends Model
{
    use HasFactory;

    protected $fillable = [
        'api_key',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    protected static function newFactory()
    {
        return \Modules\User\Database\factories\UserTokenFactory::new();
    }
}
