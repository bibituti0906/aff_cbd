<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('order')->group(function() {
    Route::post('/create', 'OrderController@store');
    Route::post('/trackingOrder/{id}', 'OrderCOntroller@trackingOrder');
    Route::middleware(['auth.jwt'])->group(function() {
        Route::get('/', 'OrderController@index');
        Route::get('/note/{id}', 'OrderController@orderNote');
        Route::get('/dashboard', 'DashboardController@index');
        Route::get('/dashboard/orderStatus', 'DashboardController@dashboardOrderStatus');
        Route::get('/dashboard/chartToDay', 'DashboardController@chartToDay');
        Route::middleware(['check_admin'])->prefix('pay')->group(function() {
           Route::get('/', 'PayController@index');
           Route::post('/payNow', 'PayController@payNow');
           Route::post('/payUser', 'PayController@payUser');
           Route::get('/viewOrderFormTo/{id}', 'PayController@viewOrderFormTo');
        });
    });
});
