<?php

namespace Modules\Order\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class OrderLog extends Model
{
    use HasFactory;

    protected $fillable = [
        'order_id',
        'logs'
    ];

    protected static function newFactory()
    {
        return \Modules\Order\Database\factories\OrderLogFactory::new();
    }
}
