<?php

namespace Modules\Order\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Order extends Model
{
    use HasFactory;

    const PENDING_PAYMENT = 0;

    const FAILED_PAYMENT = 1;

    const PROCESSING_PAYMENT = 2;

    const COMPLETTED_PAYMENT = 3;

    const ON_HOLD_PAYMENT = 4;

    const CANCEL_PAYMENT = 5;

    const REFUND_PAYMENT = 6;

    const MAP_STATUS = [
      'Pending' => 0,
      'Failed' => 1,
      'processing' => 2,
      'completed' => 3,
      'on hold' => 4,
      'cancel' => 5,
      'refund' => 6
    ];

    protected $fillable = [
        'vendor_id',
        'user_id',
        'order_status',
        'country_code',
        'first_name',
        'last_name',
        'total',
        'order_total',
        'order_tax',
        'order_refund',
        'order_aff',
        'order_commission',
        'commission_user',
        'is_pay',
        'customer_id',
        'currency'
    ];

    protected static function newFactory()
    {
        return \Modules\Order\Database\factories\OrderFactory::new();
    }
}
