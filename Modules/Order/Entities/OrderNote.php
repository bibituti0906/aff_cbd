<?php

namespace Modules\Order\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class OrderNote extends Model
{
    use HasFactory;

    protected $fillable = [
        'order_id',
        'note',
        'user_id'
    ];

    protected static function newFactory()
    {
        return \Modules\Order\Database\factories\OrderNoteFactory::new();
    }
}
