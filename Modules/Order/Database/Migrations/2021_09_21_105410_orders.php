<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Orders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('vendor_id')->index();
            $table->bigInteger('user_id')->index();
            $table->tinyInteger('order_status')->index();
            $table->string('country_code')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->double('total', 2);
            $table->double('order_total', 2);
            $table->double('order_tax', 2);
            $table->double('order_refund', 2);
            $table->double('order_commission');
            $table->double('commission_user');
            $table->tinyInteger('is_pay')->default(0)->index();
            $table->timestamps();
        });
        Schema::create('order_logs', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('order_id');
            $table->text('logs');
            $table->timestamps();
        });
        Schema::create('order_notes', function (Blueprint $table) {
            $table->id();
            $table->text('note');
            $table->bigInteger('order_id')->index();
            $table->bigInteger('user_id')->default(0)->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
