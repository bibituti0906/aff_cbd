<?php

namespace Modules\Order\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Order\Http\Requests\OrderFormRequest;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\OrderNote;
use App\Models\User;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

class PayController extends Controller
{
    public function index()
    {
        $month = request('month', date('m'));
        $year = request('year', date('Y'));
        $user = User::select(['name', 'email', 'id'])->get();
        $dataUser = $dataUserTemp = [];
        $user->map(functioN($item) use (&$dataUser) {
            $dataUser[$item->id] = [
                'id' => $item->id,
                'name' => $item->name,
                'email' => $item->email,
                'money' => 0,
                'is_pay' => 1,
                'total_order' => 0
            ];
            $dataUserTemp[] = $item->id;
        });
        $from = $year.'-'.$month.'-01';
        $to = date('Y-m-d H:i:s', strtotime('-1 second', strtotime('+1 month', strtotime($from))));
        $orderMoney = Order::where('order_status', Order::COMPLETTED_PAYMENT)->where('created_at', '>=', $from)->where('created_at', '<=', $to)
                  ->select(\DB::raw('sum(order_commission) as total'), 'user_id')
                  ->groupBy('user_id')
                  ->get();
        $orderCheckPay = Order::where('order_status', Order::COMPLETTED_PAYMENT)->where('created_at', '>=', $from)->where('created_at', '<=', $to)
                            ->where('is_pay', 0)
                            ->select(\DB::raw('count(*) as total'), 'user_id')
                            ->groupBy('user_id')
                            ->get();
        if ($orderMoney->isNotEmpty()) {
            foreach ($orderMoney as $money) {
                if (array_key_exists($money->user_id, $dataUser)) {
                    $dataUser[$money->user_id]['money'] = $money->total;
                }
            }
        }
        $orderCheckPay->map(function($item) use (&$dataUser) {
            $dataUser[$item->user_id]['total_order'] = $item->total;
            if (array_key_exists($item->user_id, $dataUser) && $item->total > 0) {
                $dataUser[$item->user_id]['is_pay'] = 0;
            }
        });
        return ['status' => true, 'data' => array_reverse($dataUser), 'from' => $from, 'to' => $to, 'month' => $month, 'year' => $year];
    }


    public function payNow()
    {
        $from = request('from', '');
        $to = request('to', '');
        if($from != '' && $to != '') {
            $orderMoney = Order::where('order_status', Order::COMPLETTED_PAYMENT)
                ->where('created_at', '>=', $from)
                ->where('created_at', '<=', $to)
                ->update(['is_pay' => 1]);
        }

        return ['status' => true];
    }

    public function payUser()
    {
        $user_id = request('user_id', '');
        $from = request('from', '');
        $to = request('to', '');
        if($from != '' && $to != '' && $user_id != '') {
            $orderMoney = Order::where('order_status', Order::COMPLETTED_PAYMENT)
                ->where('created_at', '>=', $from)
                ->where('created_at', '<=', $to)
                ->where('user_id', $user_id)
                ->update(['is_pay' => 1]);
        }
        return ['status' => true];
    }

    public function viewOrderFormTo($id)
    {
        $from = request('from', '');
        $to = request('to', '');
        $orders = Order::where('order_status', Order::COMPLETTED_PAYMENT)
            ->where('user_id', $id)
            ->where('created_at', '>=', $from)
            ->where('created_at', '<=', $to)
            ->get();
        return ['status' => true, 'data' => $orders];
    }
}
