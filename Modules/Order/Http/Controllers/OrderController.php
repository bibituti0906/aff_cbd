<?php

namespace Modules\Order\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Order\Http\Requests\OrderFormRequest;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\OrderNote;
use App\Models\User;
use Modules\User\Entities\UserCustomer;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $orders = new Order;
        $user = auth()->user();
        if ($user->user_level != User::LEVEL_USER_ADMIN) {
            $orders = $orders->where('user_id', $user->id);
        }
        if(request('order_status', '') != '') {
            $orders = $orders->where('order_status', request('order_status', ''));
        }
        if(request('order_vendor_id', '') != '') {
            $orders = $orders->where('order_vendor_id', request('order_vendor_id', ''));
        }
        if(request('from', '') != '' && request('to','') != '') {
            $orders = $orders->where('created_at', '>=', request('from'))
                ->where('created_at', '<=', request('to').' 23:59:59');
        }
        if(!empty(request('date', []))) {
            $dataDate = request('date', []);
            $orders = $orders->where('created_at', '>=', $dataDate[0]);
            $orders = $orders->where('created_at', '<=', $dataDate[1]);
        }
        $orders = $orders->orderBy('id', 'DESC')->paginate(request('limit', 40));
        return ['status' => true, 'orders' => $orders];
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('order::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(OrderFormRequest $request)
    {
        $dataForm = $request->all();

        if(isset($dataForm['ref']) || isset($dataForm['coupon'])) {
            //file_put_contents(__DIR__.'/data_aff.log', json_encode(request()->all()));
//            $findUser = User::where(function($q) use ($dataForm) {
//                if(isset($dataForm['ref'])) {
//                    $q->orWhere('ref_code', $dataForm['ref']);
//                }
//                if(isset($dataForm['coupon']) && !empty($dataForm['coupon'])) {
//                    $q->orWhereIn('coupon_referral', $dataForm['coupon']);
//                }
//
//            })->first();
            $findUser = '';
            if (isset($dataForm['coupon']) && !empty($dataForm['coupon'])) {
                $findUser = User::whereIn('coupon_referral', $dataForm['coupon'])->first();
            }
            if (isset($dataForm['ref']) && $findUser == '') {
                $findUser = User::where('ref_code', $dataForm['ref'])->first();
            }
            if (!$findUser) {
                return ['status' => false, 'msg' => 'User Not Found'];
            }
            $order = Order::where('vendor_id', $dataForm['vendor_id'])->first();
            if (!$order) {
                $order = new Order;
            }
            $dataForm['user_id'] = $findUser->id;
            $dataForm['order_commission'] = 0;
            $dataForm['commission_user'] = $findUser->commission;
            $dataForm['total'] = $dataForm['order_total'];
            $dataForm['order_total'] = $dataForm['order_total'] - $dataForm['order_tax'];
            $order->fill($dataForm);
            $order->save();
            if(request('customer_id', '') != '') {
                $userCustomer = UserCustomer::where('customer_id', request('customer_id'))->where('user_id', $findUser->id)->count();
                if ($userCustomer == 0) {
                    $userCustomer = new UserCustomer();
                    $userCustomer->fill([
                        'user_id' => $findUser->id,
                        'customer_id' => request('customer_id')
                    ]);
                    $userCustomer->save();
                    $findUser->customer = $findUser->customer + 1;
                    $findUser->save();
                }
            }
            return ['status' => true, 'order' => $order];
        }
        return ['status' => false];
    }

    public function trackingOrder(Request $request, $id)
    {
        $dataForm = $request->all();
        $order = Order::where('vendor_id', $dataForm['vendor_id'])->first();
        if ($order) {
            $dataForm['order_total'] = $dataForm['order_total'] - (isset($dataForm['order_taxt'])) ? $dataForm['order_tax'] : '';
            if ($order->user_id == 4 && $dataForm['order_status'] != Order::ON_HOLD_PAYMENT) {
                $dataForm['order_total'] = $dataForm['order_total'] - (($dataForm['order_total'] * 5)/100);
            }
            $total = $dataForm['order_total'];
            if($dataForm['order_status'] == Order::COMPLETTED_PAYMENT) {
                $dataForm['order_commission'] = ($total * $order->commission_user)/100;
            } else {
                $dataForm['order_commission'] = 0;
            }
            $dataForm['total'] = $total;
            $order->fill($dataForm);
            $order->save();
            $orderNote = new OrderNote;
            $orderNote->fill([
                'user_id' => 0,
                'order_id' => $id,
                'note' => json_encode($dataForm)
            ]);
            $orderNote->save();
            return ['status' => true, 'msg' => 'Update Tracking Success'];
        }
        return ['status' => false, 'msg' => 'Order Not Exit'];
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $order = Order::findOrFaiL($id);
        $user = auth()->user();
        if ($user->user_level != User::LEVEL_USER_ADMIN) {
            if ($user->id != $order->user_id) {
                return response()->json(['status' => true, 'msg' => 'Access Denined'],403);
            }
        }
        return ['status' => true, 'order' => $order];
    }

    public function orderNote($id)
    {
        $order = Order::where('vendor_id', $id)->first();
        $user = auth()->user();
        if ($user->user_level != User::LEVEL_USER_ADMIN) {
            if ($user->id != $order->user_id) {
                return response()->json(['status' => true, 'msg' => 'Access Denined'],403);
            }
        }
        $orderNote = OrderNote::where('order_id', $order->vendor_id)->orderBy('id', 'DESC')->get();
        return ['status' => true, 'note' => $orderNote];
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
