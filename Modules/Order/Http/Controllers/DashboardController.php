<?php

namespace Modules\Order\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Order\Http\Requests\OrderFormRequest;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\OrderNote;
use App\Models\User;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

class DashboardController extends Controller
{
    public function index()
    {
        $to = Carbon::now();
        $from = Carbon::now()->subDays(7);
        $period = CarbonPeriod::create($from, $to)->toArray();
        $period[] = date('Y-m-d');
        $dataDate = $dateIndex = $dataChart = [];
        foreach ($period as $index => $date) {
            $formatDate = Carbon::parse($date)->format('Y-m-d');
            $dataDate[] = $formatDate;
            $dateIndex[$formatDate] = $index;
            $dataChart[] = 0;
        }
        $order = Order::where('order_status', Order::COMPLETTED_PAYMENT)
            ->select( \DB::raw('sum(order_commission) as total'), \DB::raw("DATE(created_at) as date"))
            ->where('created_at', '>=', $from->format('Y-m-d'))
            ->where('created_at', '<=', $to->format('Y-m-d').' 23:59')
            ->groupBy('date');
        if (auth()->user()->user_level != User::LEVEL_USER_ADMIN) {
            $order->where('user_id', auth()->user()->id);
        }
        $order = $order->get();
        if($order->isNotEmpty()) {
            foreach ($order as $row) {
                $index = $dateIndex[$row->date];
                $dataChart[$index] = $row->total;
            }
        }
        return ['status' => true, 'dataChart' => $dataChart, 'dataDate' => $dataDate];
    }

    public function dashboardOrderStatus()
    {
        $to = Carbon::now();
        $from = Carbon::now()->subDays(7);
        $period = CarbonPeriod::create($from, $to)->toArray();
        $period[] = date('Y-m-d');
        $dataDate = $dateIndex = $dataChart = $dataIndexStaus = [];
        $i = 0;
        foreach (Order::MAP_STATUS as $k => $v) {
            $dataChart[] = [
                'name' => $k,
                'type' => 'bar',
                'stack' => 'vistors',
                'barWidth' => '60%',
                'data' => []
            ];
            $dataIndexStaus[$v] = $i;
            $i ++;
        }
        foreach ($period as $index => $date) {
            $formatDate = Carbon::parse($date)->format('Y-m-d');
            $dataDate[] = $formatDate;
            $dateIndex[$formatDate] = $index;
            foreach ($dataChart as $index => $row) {
                $dataChart[$index]['data'][] = 0;
            }
        }
        $order = Order::select( \DB::raw('count(*) as total'), \DB::raw("DATE(created_at) as date"), 'order_status')
            ->where('created_at', '>=', $from->format('Y-m-d'))
            ->where('created_at', '<=', $to->format('Y-m-d').' 23:59')
            ->groupBy('order_status','date');
        if (auth()->user()->user_level != User::LEVEL_USER_ADMIN) {
            $order->where('user_id', auth()->user()->id);
        }
        $order = $order->get();
        if ($order->isNotEmpty()) {
            foreach ($order as $row) {
                $index = $dateIndex[$row->date];
                $indexStatus = $dataIndexStaus[$row->order_status];
                $dataChart[$indexStatus]['data'][$index] = $row->total;
            }
        }
        return ['status' => true, 'dataChart' => $dataChart, 'dataDate' => $dataDate];
    }

    public function chartToDay()
    {
        $dataChart = [
          'pending' => 0,
          'onhold' => 0,
          'completed' => 0,
          'processing' => 0,
        ];
        $order = Order::whereIn('order_status', [Order::COMPLETTED_PAYMENT, Order::ON_HOLD_PAYMENT, Order::PENDING_PAYMENT, Order::PROCESSING_PAYMENT])
            ->select( \DB::raw('count(*) as total'), 'order_status')
            ->where('created_at', '>=', date('Y-m-d'))
            ->groupBy('order_status');
        if (auth()->user()->user_level != User::LEVEL_USER_ADMIN) {
            $order->where('user_id', auth()->user()->id);
        }
        $order = $order->get();
        if ($order->isNotEmpty()) {
            foreach ($order as $row) {
                if ($row->order_status == Order::COMPLETTED_PAYMENT) {
                    $dataChart['completed'] = $row->total;
                } else if ($row->order_status == Order::ON_HOLD_PAYMENT) {
                    $dataChart['onhold'] = $row->total;
                } else if ($row->order_status == Order::PENDING_PAYMENT) {
                    $dataChart['pending'] = $row->total;
                } else if ($row->order_status == Order::PROCESSING_PAYMENT) {
                    $dataChart['processing'] = $row->total;
                }
            }
        }
        return ['status' => true, 'dataChart' => $dataChart];

    }

}
