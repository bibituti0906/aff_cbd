<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class  RecaptchaService
{
    public $url = 'https://www.google.com/recaptcha';

    public function validate($token)
    {
        $privateKey  = '6Lc-n4UcAAAAANBO_FzJ1OrKHQvabYSBNbAs01Ay';
        $res = Http::asForm()->post($this->url.'/api/siteverify', [
            'secret'   => $privateKey,
            'response' => $token,
            //'remoteip' => request()->ip(),
        ]);
        $body = $res->json();
        return $body;

    }
}
