<?php

namespace App\Services;

class LogFileService
{
    private static $logPath = '../storage/logs/';

    public function writeLog($type = 'error', $message)
    {
        $fileName = storage_path('logs'.DIRECTORY_SEPARATOR.date('Y-m-d').'-'.$type.'.log');
        $openFile = fopen($fileName, 'a');
        $message = '['.date('Y-m-d H:i:s').'] '.env('APP_ENV', 'production').'.INFO: '.$message."\n";
        fwrite($openFile, $message);
        fclose($openFile);
    }
}
