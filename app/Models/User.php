<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Modules\User\Entities\UserToken;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable;

    const LEVEL_USER_ADMIN = 1;

    const LEVEL_USER_PARTNER = 2;

    const LEVEL_USER = [
        self::LEVEL_USER_ADMIN => 'Admin',
        self::LEVEL_USER_PARTNER => 'Partner'
    ];

    const USER_STATUS_DISABLE = 0;

    const USER_STATUS_ACTIVE = 1;

    const USER_STATUS = [
        self::USER_STATUS_DISABLE => 'Disable',
        self::USER_STATUS_ACTIVE => 'Active',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'user_status',
        'user_level',
        'ref_code',
        'coupon_referral',
        'cookie_duration',
        'commission',
        'visit',
        'customer'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function userToken()
    {
        return $this->hasOne(UserToken::class, 'user_id');
    }
}
