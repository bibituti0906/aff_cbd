<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Modules\User\Entities\UserAffLog;
use Modules\User\Entities\UserAffLogData;

class LogUserRefJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $userID;

    public $data;

    public function __construct($userID, array $data)
    {
        $this->userID = $userID;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->data;
        $userLog = UserAffLog::where('user_id', $this->userID)->where('report_date', date('Y-m-d'))->first();
        if (!$userLog) {
            $userLog = new UserAffLog;
            $userLog->fill([
                'user_id' => $this->userID,
                'report_date' => date('Y-m-d'),
                'total_view' => 1
            ]);
            $userLog->save();
        } else {
            $userLog->total_view += 1;
            $userLog->save();
        }
        $userLogData = new UserAffLogData;
        $userLogData->fill([
           'report_id' => $userLog->id,
           'user_id' => $this->userID,
           'ip' => array_key_exists('ip', $data) ? $data['ip'] : '',
            'user_agent' => array_key_exists('user_agent', $data) ? $data['user_agent'] : '',
            'ref_url' => array_key_exists('ip', $data) ? $data['ref_url'] : '',
        ]);
        $userLogData->save();

    }
}
